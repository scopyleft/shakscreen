# Shakscreen

Dans un souci de minimisation des coûts de maintenance, le projet est resté dans sa version Django 1.3 et Python 2.7. Les dépendances de ce projet sont directement dans le dossier `packages` du repo afin de garantir la reproductibilité dans le temps sans les dépendanaces des repo externes. 

## Prérequis

[Docker](https://docs.docker.com/desktop/install/) et [Docker Compose](https://docs.docker.com/compose/).

## Installation

`docker compose up`

En production les assets doivent être servis par Apache ou Nginx.

Exemple de config : 

    # django settings.py
    MEDIA_URL = '/static/'

    # nginx server config
    server {   
        ...
        location /static {    
            autoindex on;    
            alias /opt/aa/webroot/;    
        }
    }