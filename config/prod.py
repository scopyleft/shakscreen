from settings import *
from os import environ as env

DEBUG = env.get("DEBUG", True)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': os.path.join(PROJECT_PATH, 'data', 'db', 'sqlite.db'), # Or path to database file if using sqlite3.
    }
}

META['robots'] = 'index, follow'
GOOGLE_ANALYTICS_KEY = ''
