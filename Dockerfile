FROM python:2.7-alpine
RUN mkdir -p /run/nginx
RUN apk --no-cache add bash nginx
COPY . /app
RUN chown -R nginx:nginx /app/bucket/public/
WORKDIR /app
RUN pip install gunicorn

# Nginx Configuration
ADD nginx.conf /etc/nginx/conf.d/default.conf

ENTRYPOINT ["sh", "entrypoint.sh"]
